package main

import "fmt"

func main() {
	a := make([]int, 0, 4)
	a = append(a, 1, 2, 3)

	b := append(a, 4)

	fmt.Println("a", cap(a), len(a))
	fmt.Println("b", cap(b), len(b))
	fmt.Println(a, b)
	a[0] = 42
	fmt.Println(a, b)

	b = append(b, 5)
	a[0] = 43
	fmt.Println(a, b)
	fmt.Println("b", cap(b), len(b))
}
