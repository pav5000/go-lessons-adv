package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	file, err := os.Open("data.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	liner := NewLinerReader(file)

	data, err := ioutil.ReadAll(liner)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(data))
}

type LinerReader struct {
	lastLine int64
	rd       *bufio.Reader
	tail     string
}

func NewLinerReader(r io.Reader) *LinerReader {
	return &LinerReader{
		rd: bufio.NewReader(r),
	}
}

// func (r *LinerReader) Read(buf []byte) (int, error) {
// 	// fmt.Printf("Read call cap=%d len=%d\n", cap(buf), len(buf))
// 	n, err := r.rd.Read(buf)
// 	// fmt.Printf("   %d bytes read, err=%v\n", n, err)
// 	return n, err
// }

func (r *LinerReader) Read(buf []byte) (int, error) {
	if len(r.tail) > 0 {
		if len(r.tail) > len(buf) {
			copy(buf, []byte(r.tail[:len(buf)]))
			r.tail = r.tail[len(buf):]
			return len(buf), nil
		} else {
			copy(buf, []byte(r.tail))
			n := len(r.tail)
			r.tail = ""
			return n, nil
		}
	} else {
		line, err := r.rd.ReadString('\n')
		if err != nil {
			return 0, err
		}

		numberedLine := fmt.Sprintf("%d: %s", r.lastLine, line)
		r.lastLine++
		if len(numberedLine) > len(buf) {
			copy(buf, []byte(numberedLine[:len(buf)]))
			r.tail = numberedLine[len(buf):]
			return len(buf), nil
		} else {
			copy(buf, []byte(numberedLine))
			return len(numberedLine), nil
		}
	}
}
